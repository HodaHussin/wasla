<?php

/*
  Plugin Name: Mnbaa Galsa
  Plugin URI: http://akismet.com/
  Description: Organize ettings owner time and places .
  Version: 1.0
  Author: Mnbaa
  Author URI: Mnbaa.com
  License: GPLv2 or later
  Text Domain: galsa
 */

class MnbaaGalsa {

    private $user;
    private $point;

    function __construct() {

        add_action('init', array($this, 'galsa_create_posttype'));
        add_action('init', array($this, 'galsa_create_taxnomy'));

        add_action('admin_enqueue_scripts', array($this, "galsa_admin_scripts"));
        add_action('add_meta_boxes', array($this, 'galsa_meta_box'));
        add_filter('post_row_actions', array($this, 'galsa_action_row'), 10, 2);



        spl_autoload_register(array($this, 'galsa_autoloader'));

        // $helper = new helper_Galsa();
        $this->user = new controller_Participant();
        $this->point = new controller_Point();
    }

    /**
     * call autoload files
     * @param type $class_name
     */
    function galsa_autoloader($class_name) {
        if (false !== strpos($class_name, 'controller')) {
            $classes_dir = realpath(plugin_dir_path(__FILE__)) . DIRECTORY_SEPARATOR;
            $class_file = str_replace('_', DIRECTORY_SEPARATOR, $class_name) . '.php';
            require_once $classes_dir . $class_file;
        }
    }

    function galsa_admin_scripts() {
        wp_enqueue_style('galsa', plugins_url('/assets/css/galsa.css', __FILE__));
        wp_enqueue_style("jquery-css", plugins_url('/assets/css/jquery-ui.css', __FILE__));

        // scripts for auto complete paricipants on metting from users
        wp_enqueue_script('jquery-ui');
        wp_enqueue_script("jquery-js", plugins_url('/assets/js/jquery-ui.js', __FILE__));
        wp_localize_script('app', 'myAjax', array('ajaxurl' => admin_url('admin-ajax.php')));
        wp_enqueue_script('jquery', plugins_url('/assets/js/jquery-1.12.3.min.js', __FILE__));
        wp_enqueue_script('app', plugins_url('/assets/js/app.js', __FILE__));
        
        //include media button
        wp_enqueue_media ();
        
    }

    /**
     * create custom post mnbaa_galsa
     */
    function galsa_create_posttype() {
        register_post_type('mnbaa_galsa', array(
            'labels' => array(
                'name' => __('Mnbaa Galsa'),
                'singular_name' => __('metting'),
                'add_new' => __('Add New Meeting', 'galsa'),
                'add_new_item' => __('Add new metting', 'galsa'),
                'edit_item' => __('Edit Metting', 'galsa'),
                'new_item' => __('Add New Metting', 'galsa'),
                'view_item' => __('View Metting', 'galsa'),
                'search_items' => __('Search Metting', 'galsa'),
                'not_found' => __('No Mettings found', 'galsa'),
                'not_found_in_trash' => __('No Mettings found in trash', 'galsa'),
            ),
            'show_ui' => true,
            'public' => true,
            'rewrite' => array('slug' => 'galsa'),
            'supports' => array('title'),
                )
        );
    }

    /**
     * 
     * create meta box for custm post galsa
     */
    function galsa_meta_box() {
        if (isset($_GET['post'])) {
            add_meta_box('post_media', __('Metting Details', 'galsa'), array($this, 'view'), 'mnbaa_galsa');
        }
    }

    /**
     * view all points for this custom post mnbaa_galsa by fetching metting_point_map
     * meta data 
     */
    function view() {
        global $wpdb;
        if (isset($_GET['post'])) {
            $post_id = $_GET['post'];
            $points = get_post_meta($post_id, "metting_point_map", TRUE);
        }
        include_once plugin_dir_path(__FILE__) . '/views/points_view.php';
    }

    /**
     * add custom link view points  for each metting
     * @param type $actions
     * @param type $post
     * @return type
     */
    function galsa_action_row($actions, $post) {
        if ($post->post_type == "mnbaa_galsa") {
            unset($actions['inline hide-if-no-js']);
            unset($actions['view']);
        }
        return $actions;
    }

    /**
     * register taxnomy 
     */
    function galsa_create_taxnomy() {

        register_taxonomy(
                'department', 'mnbaa_galsa', array(
            'label' => __('Department'),
            'rewrite' => array('slug' => 'department'),
            'hierarchical' => true,
                )
        );
        register_taxonomy(
                'tag', 'mnbaa_galsa', array(
            'label' => __('Tag'),
            'rewrite' => array('slug' => 'tag'),
            'hierarchical' => true,
                )
        );
        register_taxonomy(
                'place', 'mnbaa_galsa', array(
            'label' => __('Place'),
            'rewrite' => array('slug' => 'place'),
            'hierarchical' => true,
                )
        );
    }

}

$galsa = new MnbaaGalsa();


