<?php

class controller_Participant {

    function __construct() {


        add_action('admin_menu', array($this, 'galsa_add_sub_menu'));
        add_action('admin_post_add_participant', array($this, 'save'));
        //for get users in ajax
        add_action('wp_ajax_get_users', array($this, 'get_users'));
        add_action('wp_ajax_email_exist', array($this, 'email_exist'));
    }

    function galsa_add_sub_menu() {
        add_submenu_page(
                'edit.php?post_type=mnbaa_galsa', 'participants', 'Participants', 'manage_options', 'participants-page', array($this, "route_paricipant"));
    }

    function galsa_paricipant_create_posttype() {
        register_post_type('galsa_users', array(
            'labels' => array(
                'name' => __('Mnbaa Galsa'),
                'singular_name' => __('user'),
                'add_new' => __('Add New Meeting', 'galsa'),
                'edit_item' => __('Edit Metting', 'galsa'),
                'new_item' => __('Add New Metting', 'galsa'),
                'view_item' => __('View Metting', 'galsa'),
                'search_items' => __('Search Metting', 'galsa'),
                'not_found' => __('No Mettings found', 'galsa'),
                'not_found_in_trash' => __('No Mettings found in trash', 'galsa'),
            ),
            'show_ui' => true,
            'public' => false,
            'rewrite' => array('slug' => 'galsa'),
            'supports' => array('title'),
                )
        );
    }

    function view($page_name = null) {

        if (isset($page_name) && $page_name != null) {
            //die($page_name);
            include_once plugin_dir_path(__FILE__) . '../views/' . $page_name . '.php';
        } else {
            include_once plugin_dir_path(__FILE__) . '../views/participants.php';
        }
    }

    //add user to custom post galsa_users
    function route_paricipant() {
        if (isset($_POST) && isset($_POST['galsa_users'])) {
            $this->save();
        } elseif (isset($_POST) && isset($_POST['galsa_user_edit'])) {
            $this->update();
        } elseif (isset($_GET['action']) && ($_GET['action'] == 'add' || $_GET['action'] == 'edit')) {
            $this->view('userAdd');
        } else {
            $this->view();
        }
    }

    function save() {

        if (isset($_POST) && isset($_POST['action']) && $_POST['action'] == 'add_participant') {

            if (isset($_POST['username']) && !empty($_POST['username'])) {
                $my_post = array(
                    'post_title' => $_POST['username'],
                    'post_type' => 'galsa_users',
                    'post_status' => 'publish'
                );


                $post_id = wp_insert_post($my_post);
                $user_meta = $_POST['user'];
                $valid = TRUE;
                if (!preg_match('/[^0-9]/', $user_meta['phone'])) {
                    foreach ($user_meta as $meta_key => $value) {
                        update_post_meta($post_id, $meta_key, $value);
                    }
                } else {
                    $valid = FALSE;
                    echo( "phone is invalid" );
                    wp_redirect(admin_url() . "edit.php?post_type=mnbaa_galsa&page=participants-page&action=add&msg='phone_fail");
                }
            } else {
                $valid = FALSE;
                echo "enter your name";
                wp_redirect(admin_url() . "edit.php?post_type=mnbaa_galsa&page=participants-page&action=add&msg='name_fail");
            }
            //
            if ($valid === TRUE) {
                wp_redirect(admin_url() . "edit.php?post_type=mnbaa_galsa&page=participants-page");
                exit;
            }
        } elseif (isset($_POST) && isset($_POST['galsa_user_edit'])) {
            $this->update();
        }
    }

    function update() {



        $my_post = array(
            'ID' => $_POST['galsa_user_edit'],
            'post_title' => $_POST['username'],
            'post_status' => 'publish'
        );

        $post_id = wp_update_post($my_post);
        $user_meta = $_POST['user'];
        foreach ($user_meta as $meta_key => $value) {
            update_post_meta($post_id, $meta_key, $value);
        }
        wp_redirect(admin_url() . "edit.php?post_type=mnbaa_galsa&page=participants-page");
        exit;
    }

    function get_users() {
        $term = $_GET['term'];
        $participants = array();
        $type = 'galsa_users';
        $args = array(
            'post_type' => $type,
            'post_status' => 'publish',
            's' => $term,
        );

        $my_query = new WP_Query($args);
        if ($my_query->have_posts()) {
            while ($my_query->have_posts()) :
                $my_query->the_post();

                $participants[get_the_ID()] = get_the_title(get_the_ID());

            endwhile;
        }
        echo json_encode($participants);
        die();
    }

    function email_exist() {
        $term = $_GET['mail'];
        global $wpdb;
        $values = $wpdb->get_col($wpdb->prepare("SELECT meta_value FROM $wpdb->postmeta WHERE meta_key = %s ", "email"));
        return in_array($term, $values);
        die();
    }

}
