<?php

class controller_Point {

    function __construct() {

        add_action('init', array($this, 'save_new_point'));
        add_action('wp_ajax_get_point_data_ajax', array('controller_Point', 'get_point_data_ajax'));
        add_action('wp_ajax_delete_by_ajax', array('controller_Point', 'delete_by_ajax'));
    }

    /**
     * save new point data from dialog box popup
     */
    function save_new_point() {

        if ($_POST && isset($_POST['dialog_galsa_nonce'])) {
            $post_id = $_POST['post_id'];

            if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
                return $post_id;
            }

            if (!empty($_POST['new_point'])) {
                $point_data = $_POST['new_point'];
                if (empty($point_data['title'])) {
                    return $post_id;
                }
                //update point data
                if (isset($_POST['point_id']) && !empty($_POST['point_id'])) {
                    $point_id = $_POST['point_id'];
                    $this->update_point($point_id, $point_data);
//                    
                }
                // add new point
                else {
                    $point_id = add_post_meta($post_id, "point_data", $point_data);
                    $main_point_id = '';
                    if (isset($point_data['basic_point']) && $point_data['basic_point'] != '') {
                        $main_point_id = $point_data['basic_point'];
                    }
                    $this->update_points_map($post_id, $point_id, $main_point_id);
                }
            }
        }
    }

    /**
     * update metting_point_map when insert new point or sub point
     * @param type $post_id
     * @param type $point_id
     * @param type $main_point_id
     */
    function update_points_map($post_id, $point_id, $main_point_id = NULL) {

        $map = get_post_meta($post_id, "metting_point_map", true);
        //  maping main point
        if ($main_point_id == NULL) {
            $map[$point_id] = array();
        }
        // mapping sub point 
        else {
            foreach ($map as $key => $node) {
                if ($main_point_id == $key) {
                    array_push($map[$key], $point_id);
                }
            }
        }
        update_post_meta($post_id, "metting_point_map", $map);
    }

    /**
     * get data from meta point data and unserialize it to array to
     * view them with thier order for each metting
     * @global type $wpdb
     * @param type $point_id
     * @return type integer
     */
    function get_point_data($point_id) {
        global $wpdb;
        $result = $wpdb->get_row('SELECT * FROM ' . $wpdb->prefix . 'postmeta WHERE meta_id=' . $point_id);
        $point = unserialize($result->meta_value);
        return $point;
    }

    /**
     * get point data to view them on click edit button fro each point on popup
     * @return type json
     * @global type $wpdb
     */
    function get_point_data_ajax() {

        global $wpdb;
        $point_id = $_GET['id'];
        $result = $wpdb->get_row('SELECT * FROM ' . $wpdb->prefix . 'postmeta WHERE meta_id=' . $point_id);
        $point = unserialize($result->meta_value);
        //get attachment for this point if exist
        $attachment_id = $point['attachment'];
        if (!empty($attachment_id)) {
            $file = get_post($attachment_id);

//            $attach = $wpdb->get_row('SELECT * FROM ' . $wpdb->prefix . 'postmeta WHERE meta_id=' . $attachment_id);
//            $attach = unserialize($attach->meta_value);
            //add attachment url to point data
            $point["attachment_url"] = $file->guid;
        }
        echo json_encode($point);
        die();
    }

    /**
     * delete point when click on remove link  next to each point title
     * @global type $wpdb
     */
    function delete_by_ajax() {

        global $wpdb;
        $point_id = $_POST['id'];
        $post_id = $_POST['post_id'];
        $parent_id = $_POST['parent_id'];
        $table = $wpdb->prefix . 'postmeta';
        //
        $map = get_post_meta($post_id, 'metting_point_map', TRUE);
        //delete sub point
        if (!empty($parent_id)) {
            foreach ($map[$parent_id] as $key => $val) {
                if ($val == $point_id) {
                    unset($map[$parent_id][$key]);
                }
            }
        }
        //delete main point with all sub points 
        else {
            unset($map[$point_id]);
        }
        $map = update_post_meta($post_id, "metting_point_map", $map);
        //
        $result = $wpdb->delete($table, array('meta_id' => $point_id));
        echo json_encode($result);
        die();
    }

    /**
     * update point meta data
     * @global type $wpdb
     * @param type $id
     * @param type $value
     * @return type
     */
    function update_point($id, $value) {
        global $wpdb;
        $value = serialize($value);
        $result = $wpdb->update(
                $wpdb->prefix . 'postmeta', array(
            'meta_value' => $value, // string
                ), array('meta_id' => $id), array(
            '%s', // value1
            '%d' // value2
                ), array('%d')
        );
        return $result;
    }

}
