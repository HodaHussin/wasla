<?php

echo sprintf('<h2 ><a href="?post_type=mnbaa_galsa&page=%s&action=%s">' . __("Add new user", 'galsa') . '</a></h2>', $_REQUEST['page'], 'add');
?>
<table class="widefat forms-codes">
    <thead>
        <tr>
            <th scope="col" style=""><?php _e('Name', 'galsa'); ?></th>
            <th scope="col" style=""><?php _e('Email', 'galsa'); ?></th>
            <th scope="col" style=""><?php _e('Phone', 'galsa'); ?></th>
            <th scope="col" style=""><?php _e('Edit', 'galsa'); ?></th>
            <th scope="col" style=""><?php _e('Delete', 'galsa'); ?></th>

        </tr>
    </thead>


    <?php
    $args = array(
        'post_type' => 'galsa_users',
    );
    $loop = new WP_Query($args);

    while ($loop->have_posts()): $loop->the_post();

        echo '<tr><td>';
        the_title();
        echo '</td><td>' . get_post_meta(get_the_ID(), 'email', TRUE) . '</td>';
        echo '<td>' . get_post_meta(get_the_ID(), 'phone', TRUE) . '</td>';
        ?>
        <td><a href="?page=<?php echo $_REQUEST['page'] ?>&post_type=mnbaa_galsa&action=delete&id=<?php the_ID() ?>"
               onclick="return confirm(' you want to delete?');"
               ><?php _e('Delete', 'galsa'); ?></a></td>
        <td><a href="?page=<?php echo $_REQUEST['page'] ?>&post_type=mnbaa_galsa&action=edit&id=<?php the_ID() ?>"
               ><?php _e('Edit', 'galsa'); ?></a></td>
    </tr>

    <?php
endwhile;
wp_reset_query();
?>



</table>

