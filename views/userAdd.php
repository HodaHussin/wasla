<?php
if (isset($_GET['msg']) && $_GET['msg'] == 'name_fail') {
    ?>

<div class="error"><p>Enter the name of participant</p></div>
    <?php
}
if (isset($_GET['msg']) && $_GET['msg'] == 'phone_fail') {
    ?>

<div class="error"><p>Enter correct phone number</p></div>
    <?php
}
?>
<h2>Add User </h2>
<form action="<?php echo admin_url(); ?>admin-post.php?action=add_participant" method="post" id="participants_form"  >
    <?php if (isset($_GET['id'])) { ?>
    <input type="hidden" name="galsa_user_edit"  id ="edit_user" value="<?php echo $_GET['id'] ?> "/>
      
    <?php } else { ?>
           <input type="hidden" name="action" value="add_participant">
    <?php } ?>
    <table>
        <tr>
            <td>
                <span>UserName</span>
            </td><td>
                <span><input type="text" value="<?php if (isset($_GET['id'])) echo get_the_title($_GET['id']); ?>" name="username"></span>
            </td>
        </tr>


        <tr>
            <td>
                <span>Email</span>
            </td><td>
                <span><input  id="galsa_user_email" type="email" value="<?php if (isset($_GET['id'])) echo get_post_meta($_GET['id'], 'email', true); ?>" name="user[email]"></span>
            </td>
        </tr>

        <tr>
            <td>
                <span>Phone</span>
            </td><td>
                <span><input type="text" value="<?php if(isset($_GET['id'])) echo  get_post_meta($_GET['id'], 'phone', true); ?>" name="user[phone]"></span>
            </td>
        </tr>

        <tr>
            <td>
                <input type="submit" value="Add user" />
            </td>
        </tr>
    </table>
</form>



