<a href="#" class="add_point"> <?php _e('Add new point', 'galsa'); ?></a>
<!--<input type="hidden" name="wp_custom_galsa_nonce">
<input type="hidden" name="wp_custom_attachment_nonce">-->
<div title="Add  Point" >

    <form method="get"></form>
    <div id="dialog" class="point" title="Add new" >

        <form method="post" action="" enctype="multipart/form-data" id="galsa_form">
            <input type="hidden" name="dialog_galsa_nonce">
            <input type="hidden" name="dialog_attachment_nonce">
            <input type="hidden" name="post_id" value="<?php if (isset($_GET['post'])) echo $_GET['post']; ?>">
            <input type="hidden" name="point_id" id="point_id" value="<?php if (isset($_GET['post'])) echo $_GET['post']; ?>">
            <input type="hidden" name="new_point[basic_point]" id="parent_id" value="">
            <table  id="base_table">

                <tr>
                    <td>
                        <span>Title</span>
                    </td><td>
                        <span><input id="title" type="text" value="" name="new_point[title]"></span>
                    </td>
                </tr>
                <tr>    
                    <td>
                        <span>Description</span>
                    </td><td>
                        <span><textarea cols="35"  rows="3" id="description" name="new_point[description]"></textarea></span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span>Attachment</span>
                    </td>
                    <td>
<!--                        <input type="file" id="wp_custom_attachment" name="new_wp_custom_attachment" />
                        <p class="description">Upload your Audio here</p>-->

                        <p>
                            <input type="hidden"  name="new_point[attachment]" value="" class="regular-text process_custom_images galsa-attach" id="attachment" name="" max="" min="1" step="1">
                            <button class="set_custom_images button"><span class="wp-media-buttons-icon"> upload Media </span></button>
                            <a href="javascript:(0);" class="remove-attach">remove audio</a>
                        </p>
                        <p id="attachment_url"></p>

                    </td>

                </tr>
                <tr>
                    <td>
                        <span>Participant</span>
                    </td><td>
                        <span><input type="text" id="participant" value="" name="new_point[participant]"></span>
                    </td>
                </tr>
            </table>
            <input type="submit"  id='add_dialog' value="Add Point"  />
        </form>
    </div>
</div>

<?php if (isset($points) && !empty($points)) { ?>
    <div id="accordion">
        <?php
        foreach ($points as $key => $val) {
            $point = $this->point->get_point_data($key);
            ?>
            <section class="single-item">
                <h3><?php echo $point['title']; ?> 
                    <div class="actions">
                        <span class="add add_point" data-id="<?php echo $key; ?>">Edit</span>
                        <span class="remove"  data-id="<?php echo $key; ?>" >Remove</span>
                        <?php
                        if (isset($point['attachment']) && !empty($point['attachment'])) {
                            $file = get_post($point['attachment']);
                            ?>
                            <span class="play"></span>
                            <audio controls  class="galsa_audio">
                                <source src="<?php echo $file->guid; ?>"  type="<?php echo $file->post_mime_type ?>" >
                            </audio>
        <?php } ?>
                    </div>
                </h3>

                <div class="accordion-div">
                    <div id="point-content">
                        <p><?php echo $point['participant']; ?></p>
                        <p><?php echo $point['description']; ?></p>
                    </div>
                    <!-- if there is subpoint for this point ---->
                        <?php if (!empty($val)) { ?>
                        <div id="accordion2" >
                            <?php
                            foreach ($val as $sub_key => $sub_value) {
                                $point = $this->point->get_point_data($sub_value);
                                if (isset($point['attachment']) && !empty($point['attachment'])) {
                                    $sub_file = get_post($point['attachment']);
                                }
                                ?>

                                <section class="single-item">
                                    <h3><?php echo $point['title'] ?> 
                                        <div class="actions">

                                            <span class="add add_point" data-id="<?php echo $sub_value; ?>">Edit</span>
                                            <span class="remove"  parent-id="<?php echo $key; ?>" data-id="<?php echo $sub_value; ?>">Remove</span>
                                            <?php
                                            if (isset($point['attachment']) && !empty($point['attachment'])) {
                                                ?>
                                                <span class="play"></span>
                                                <audio controls class="galsa_audio" style="display: none">
                                                    <source src="<?php echo $sub_file->guid; ?>"  type="<?php echo $sub_file->post_mime_type ?>" >
                                                </audio>
                <?php } ?>


                                        </div>
                                    </h3>

                                    <div class="accordion-div">
                                        <div id="point-content">
                                            <p><?php echo $point['participant']; ?></p>
                                            <p><?php echo $point['description']; ?></p>
                                        </div>
                                    </div>
                                </section>

                        <?php } ?>
                        </div>
        <?php } ?>

                    <a href="#" class="sub_add_point" data-parent="<?php echo $key ?>" > <?php _e('Add Subpoint', 'galsa'); ?></a>
                </div>
            </section>
            <?php
        }
        ?></div>
<?php }
?>




