<input type="hidden"  value="<?php //echo $key  ?>"name="point_id[]" />
<?php $key += 1; ?>
<table  id="base_table">
    <?php if (isset($main_points) && !empty($main_points)) { ?>
        <tr>
            <td>
                <span>Basic point</span>
            </td>
            <td>
                <select name="point[<?php echo $key ?>][basic_point]">
                    <option value=""><?php _e('Select main point') ?></option>
                    <?php
                    foreach ($main_points as $basic_key => $basic_val) {
                        ?>
                        <option  value="<?php echo $basic_key ?>"><?php echo $basic_val['title'] ?></option>
                    <?php } ?>

                </select>

            </td>
        </tr>
    <?php } ?>
    <tr>
        <td>
            <span>Title</span>
        </td><td>
            <span><input type="text" value="<?php echo $point['title'] ?>" name="point[<?php echo $key ?>][title]"></span>
        </td>
    </tr>
    <tr>    
        <td>
            <span>Description</span>
        </td><td>
            <span><textarea name="point[<?php echo $key ?>][description]"><?php echo $point['description'] ?></textarea></span>
        </td>
    </tr>
    <tr>
        <td>
            <span>Attachment</span>
        </td><td>
            <p class="description">Upload your Audio here</p>
            <input type="file" id="wp_custom_attachment" name="wp_custom_attachment" value="" size="25" />
        </td>
    </tr>
    <tr>
        <td>
            <span>Participant</span>
        </td><td>
            <span><input type="text" id="participant" value="<?php echo $point['participant'] ?>" name="point[<?php echo $key ?>][participant]"></span>
        </td>
    </tr>
</table>
