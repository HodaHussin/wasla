<?php

class helper_Galsa {

    /**
     * call views and send data
     * @param type $file_name
     * @param type $data
     */
    function galsa_view($file_name, $data = null) {
        require_once plugin_basename('mnbaa_galsa/views/' . $file_name . '.php');
    }

}
