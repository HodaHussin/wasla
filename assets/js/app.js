jQuery(document).ready(function () {
    var $ = jQuery;

    jQuery("#participant").autocomplete({
        appendTo: "#galsa_form",
        source: ajaxurl + '?action=get_users',
        minLength: 2,
        select: function (event, ui) {
            // jQuery('#user_result').val(ui.item.id);
        },
        change: function (event, ui) {
            if (ui.item == null) {
                jQuery("#participant").val('');
                jQuery("#participant").focus();
            }
        },
        messages: {
            noResults: '',
            results: function () {
            }
        }
    });

    jQuery("#participants_form").submit(function (e) {
        if ($("#participants_form").find("#edit_user").length === 0) {
            $isValid = true;
            email = $("#galsa_user_email").val();
            $.ajax({
                url: ajaxurl + '?action=email_exist',
                data: {mail: email},
                dataType: "json",
                success: isValid,
            });

        }
    });

    function isValid() {

        alert("response");
        if ($isValid == false) {
            e.preventDefault();
            return false;
        }
    }


    $('#accordion h3 span.remove').on('click', function () {
        var node = $(this);
        var res = confirm("Are you sure to delete this point?");
        if (res == true) {
            point_id = $(this).attr('data-id');
            post_id = $('#post_ID').val();
            parent_id = $(this).attr('parent-id');

            $.ajax({
                url: ajaxurl + '?action=delete_by_ajax',
                dataType: "json",
                method: "post",
                data: {id: point_id, post_id: post_id, parent_id: parent_id},
                success: function (response) {
                    if (response === 1) {
                        node.closest('.single-item').fadeOut('slow', function () {
                            $(this).remove();
                        });
                        alert("Point Succesfully deleted");
                    } else
                        alert("Deleted failed");
                }
            });
        }

    });

    $('#accordion h3 span').click(function (event) {
        event.stopPropagation();
        event.preventDefault();
    });

    $(".add_point,.sub_add_point").on('click', function () {
        $("#dialog :input").not("input[type='hidden']").val("");
        //get data by point id to view when edit
        point_id = $(this).attr('data-id');
        $("#point_id").val(point_id);

        parent_id = $(this).attr('data-parent');
        $("#parent_id").val(parent_id);

        if (point_id != undefined || this.className != 'add_point')
        {
//            alert(point_id);
            data = get_point_data(point_id);
        }
        $("#dialog").dialog({
            height: 430,
            width: 500,
            buttons: {
                Add: function () {
                    $('#galsa_form').submit();
                    //save_point_ajax(point_id);
                },
                Cancel: function () {
                    $(this).dialog("destroy");
                },
            }
        });
        //$("#dialog").css('display', 'block');
    });

    //
    function get_point_data(point_id) {
        $.ajax({
            url: ajaxurl + '?action=get_point_data_ajax',
            dataType: "json",
            data: {id: point_id},
            success: function (response) {
                // console.log(response[key]);
                //alert(response);
                for (obj in response) {
                    if (obj == "attachment_url") {
                        $('#galsa_form ').find('#' + obj).html(response[obj]);
                    } else {
                        $('#galsa_form ').find('#' + obj).val(response[obj]);
                    }
                }
            }

        })
    }

    $(function () {
        $("#accordion,#accordion2").accordion({
            header: "> section.single-item > h3",
            heightStyle: "content",
            autoOpen: true,
        });

    });

    $(".play").on('click', function () {
        audio = $(this).parent().find('.galsa_audio');
        if (audio.css('display') == 'none') {
            audio.css('display', 'block');
            audio[0].play();
        }
        else {
            audio.css('display', 'none');
            audio[0].pause();
        }


    });

    //media button 
    if ($('.set_custom_images').length > 0) {
        if (typeof wp !== 'undefined' && wp.media && wp.media.editor) {
            $('.set_custom_images').on('click', function (e) {
                e.preventDefault();
                var button = $(this);
                var id = button.prev();
                wp.media.editor.send.attachment = function (props, attachment) {
                    id.val(attachment.id);
                };
                wp.media.editor.open(button);
                return false;
            });
        }
    };
    
    //remove attachment
    $(".remove-attach").on('click',function(){
//        alert($(this).siblings("#attachment_url").html());
        $(this).parent().siblings("#attachment_url").html("");
        $(this).siblings('.galsa-attach').val("");
    });


});